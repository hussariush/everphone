export interface Device {
    "id": number,
    "manufacturer": string,
    "name": string,
    "color": string,
    "edition": string,
    "image_url": string,
    "price": {
        "amount": number,
        "currency": string
    },
    "storage": string,
    "ram": string,
    "display_size": string,
    "accessory_ids": number[]
}

export interface DevicesCatalog {
        "devices": Device[],
        "accessories": [
            {
                "id": number,
                "manufacturer": string,
                "name": string,
                "color": string,
                "edition": string,
                "image_url": string,
                "price": {
                    "amount": number,
                    "currency": string
                }
            }
        ],
        "phone_plans": [
            {
                "id": number,
                "name": string,
                "provider": string,
                "included_data_volume_in_gb": number,
                "price": {
                    "amount": number,
                    "currency": string
                }
            }
        ],
        "pagination": {
            "first": string,
            "last": string,
            "prev": string,
            "next": string
        }
    }


const res: DevicesCatalog = {
    "devices": [
        {
            "id": 0,
            "manufacturer": "abc",
            "name": "Fairphone",
            "color": "blue",
            "edition": "latest",
            "image_url": 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Fairphone_2_(25250139694)_(cropped).jpg/1200px-Fairphone_2_(25250139694)_(cropped).jpg',
            "price": {
                "amount": 110,
                "currency": "EUR"
            },
            "storage": "sd",
            "ram": "12gb",
            "display_size": "1024",
            "accessory_ids": [
                0
            ]
        },
        {
            "id": 2,
            "manufacturer": "abc2",
            "name": "Fairphone",
            "color": "red",
            "edition": "latest",
            "image_url": 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Fairphone_2_(25250139694)_(cropped).jpg/1200px-Fairphone_2_(25250139694)_(cropped).jpg',
            "price": {
                "amount": 1410,
                "currency": "EUR"
            },
            "storage": "sd",
            "ram": "12gb",
            "display_size": "1024",
            "accessory_ids": [
                0
            ]
        },
        {
            "id": 3,
            "manufacturer": "abc3",
            "name": "Fairphone",
            "color": "green",
            "edition": "latest",
            "image_url": 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Fairphone_2_(25250139694)_(cropped).jpg/1200px-Fairphone_2_(25250139694)_(cropped).jpg',            
            "price": {
                "amount": 1110,
                "currency": "EUR"
            },
            "storage": "sd",
            "ram": "12gb",
            "display_size": "1024",
            "accessory_ids": [
                0
            ]
        }
    ],
    "accessories": [
        {
            "id": 0,
            "manufacturer": "string",
            "name": "string",
            "color": "string",
            "edition": "string",
            "image_url": "string",
            "price": {
                "amount": 0,
                "currency": "EUR"
            }
        }
    ],
    "phone_plans": [
        {
            "id": 0,
            "name": "Cheap",
            "provider": "o2",
            "included_data_volume_in_gb": 10,
            "price": {
                "amount": 10,
                "currency": "EUR"
            }
        }
    ],
    "pagination": {
        "first": "string",
        "last": "string",
        "prev": "string",
        "next": "string"
    }
}

export default res;