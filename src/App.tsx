import React from 'react';
import './App.css';
import Dashboard from './pages/Dashboard/Dashboard';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from "react-router-dom";
import Details from './pages/Details/Details';


function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/details/:deviceId" render={(props) => <Details {...props} />} />
          <Route path="/">
            <Dashboard />
          </Route>
        </Switch>
      </Router>
    </div >
  );
}

export default App;
