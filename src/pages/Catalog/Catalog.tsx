import React, { FC, useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { fetchDevices } from '../../apis/devices.api';
import Card from '../../components/Card/Card';
import { catalogUrl } from '../../Const/deviceApi';
import { Device, DevicesCatalog } from '../../Const/testData';
import './styles.css'

const Catalog: FC<any> = () => {
    const testUserId = '123';
    const [data, setData] = useState<DevicesCatalog>({} as DevicesCatalog);
    const [error, setError] = useState<{} | null>(null);

    const getDevices = async () => {
        const baseUrl = catalogUrl(testUserId);
        try {
            const res = await fetchDevices(baseUrl);
            setData(res as DevicesCatalog);
        } catch (error) {
            console.warn(error)
            setError(error)
        }
    }

    useEffect(() => {
        getDevices();
    }, [])

    return (
        <>
            {console.log(`devices`, data)}
            {error && <p>{error}</p>}
            <ul className='catalog'>
                {data?.devices && data.devices.map((device: Device) => (
                    <li>
                        <Link to={`/details/${device.id}`}>
                            <Card name={device.name} image_url={device.image_url} price={device.price} />
                        </Link>
                    </li>

                ))}
            </ul>
        </>
    )
}

export default Catalog;
