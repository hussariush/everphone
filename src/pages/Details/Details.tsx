import React, { FC, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import DeviceDetails from '../../components/DeviceDetails/DeviceDetails';
import res, { Device } from '../../Const/testData';

const Details: FC<any> = (props) => {
    const { deviceId } = useParams<any>();
    const [deviceDetails, setDeviceDetails] = useState<Device | undefined>(undefined)
    
    // this would come from global storage or more specific request with a device id specified
    const getDeviceDetails = async () => {
        const newDevice = res.devices.find(device => device.id === +deviceId);
        setDeviceDetails(newDevice)    
    }

    useEffect(() => {
        getDeviceDetails();
    }, [])

    return (
        <>
            {deviceDetails && <DeviceDetails {...deviceDetails}/>}
        </>
    )
}

export default Details;
