import React, { FC } from 'react'
import { Device } from '../../Const/testData';
import './styles.css';

const DeviceDetails: FC<any> = ({ name, image_url, price, manufacturer, color, edition }: Device) => {

    return (
        <div className='detailsCard'>
            <picture>
                <img className='detailsCard__image' src={image_url} alt={name} />
                <figcaption className='detailsCard__imageCaption'>{name}</figcaption>
            </picture>
            <div className='detailsCard__descriptionWrapper'>
                <b className='detailsCard__emphasis'>{price.amount} {price.currency}</b>
                <span className='detailsCard__manufacturer'>Make: {manufacturer}</span>
                <span className='detailsCard__color'>Colour: {color}</span>
                <span className='detailsCard__edition'>Edition: {edition}</span>
            </div>
        </div>
    )
}

export default DeviceDetails
