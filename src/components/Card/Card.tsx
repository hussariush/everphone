import React, { FC } from 'react'
import './styles.css';

interface CardData {
    name: 'string';
    price: { [key: string]: string };
    image_url: 'string'
}

const Card: FC<any> = ({
    name,
    image_url,
    price }: CardData) => {
    return (
        <div className='card'>
            <picture>
                <img className='card__image' src={image_url} alt={name} />
                <figcaption className='card__imageCaption'>{name}</figcaption>
            </picture>
            <b className='card__emphasis'>{price.amount} {price.currency}</b>
        </div>
    )
}

export default Card
